using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreRankingBoard : MonoBehaviour
{
    [Header("Ranking Board")]
    public Transform Rankinganchor;
    public GameObject RankingPrefab;


    private void OnEnable()
    {
        ScoreFormRankingData(RankingScoreManager.Load());
    }

    public void ScoreFormRankingData(SaveData saveData)
    {
        for(int i = 0;i < saveData.users.Count; i++)
        {
            var user = saveData.users[i];
            var go = Instantiate(RankingPrefab, Vector3.zero, Quaternion.identity,Rankinganchor);
            go.SetActive(true);
            go.GetComponent<ScoreItem>().SetPosAndScore(i, user.score);
        }
    }
  /*  private void DeleteItems()
    {
        foreach (var item in Rankinganchor.GetComponentInChildren<ScoreItem>())
            Destroy(item.gameObject);
    }*/
}
