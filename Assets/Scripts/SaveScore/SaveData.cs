using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ScoreData
{
    public int score;

}
[System.Serializable]
public class SaveData 
{
    public List<ScoreData> users = new List<ScoreData>();
    public void AddScoreData(ScoreData newdata)
    {
        users.Add(newdata);
    }
    public void SortDesc()
    {
        users.Sort((A, B) => B.score.CompareTo(A.score));
    }
}
