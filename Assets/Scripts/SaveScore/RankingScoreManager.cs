using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class RankingScoreManager
{
    public static string directory => Application.dataPath + "/save";
    public static string fileName = "ScoreData.json";

    static string filePath = Path.Combine(directory, fileName);


    public static void SaveScore (int score)
    {
        var loadedData = Load();
        loadedData.AddScoreData(new ScoreData { score = score });
        loadedData.SortDesc();
        Save(loadedData);
    }

    public static void Save(SaveData data)
    {
        string dir = directory;
        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);
        string json = JsonUtility.ToJson(data);
        File.WriteAllText(filePath, json);
        
    }
    public static SaveData Load()
    {
        SaveData data = new SaveData();
        if (File.Exists(filePath))
        {
            string json = File.ReadAllText(filePath);
            data = JsonUtility.FromJson<SaveData>(json);
        }
        else
        {
            Debug.Log("Save File does not exits");
        }
        return data;
    }
}
