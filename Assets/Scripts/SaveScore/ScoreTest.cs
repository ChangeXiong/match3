using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTest : MonoBehaviour
{
    public SaveData rankingData;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            RankingScoreManager.Save(rankingData);
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            rankingData = RankingScoreManager.Load();
        }
    }
}
