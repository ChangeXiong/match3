using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreItem : MonoBehaviour
{
    [SerializeField] private Text Pos;
    [SerializeField] private Text Score;

    public void SetPosAndScore(int pos,int score)
    {
        Pos.text = (pos + 1).ToString();
        Score.text = score.ToString();
    }
}
