using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Text))]
public class ScoreManager : Singleton<ScoreManager>
{
    private MatchablePool pool;
    private MatchableGrid grid;
    private AudiioMixer audioMixer;

    [SerializeField]
    private Transform collectionPoint;
    [SerializeField]
    private Text scoreText,
                 comboText;
    // UI slider element for displaying the time remaining in the combo
    [SerializeField]
    private Image comboSlider;

    private int score,
                comboMultiplier;
    public int Score
    {
        get { return score; }
    }
    private float timeSinceLastScore;

    [SerializeField]
    private float maxComboTime,
                  currentComboTime;

    private bool timerIsActive;
/*    protected override void Init()
    {
        scoreText = GetComponent<Text>();
        
    }*/
    private void Start()
    {
        pool = (MatchablePool)MatchablePool.Instance;
        grid = (MatchableGrid)MatchableGrid.Instance;
        audioMixer = AudiioMixer.Instance;

        comboText.enabled = false;
        comboSlider.gameObject.SetActive(false);
    }
    public void Reset()
    {
        score = 0;
        scoreText.text = score.ToString();
        timeSinceLastScore = maxComboTime;
    }
    public void AddScore(int amount)
    {
        score += amount * IncreaseCombo();
        scoreText.text = score.ToString();

        timeSinceLastScore = 0;
        if (!timerIsActive)
            StartCoroutine(ComboTimer());
        // Play score sound
        audioMixer.PlaySound(SoundEffects.score);
    }
    private IEnumerator ComboTimer()
    {
        timerIsActive = true;
        comboText.enabled = true;
        comboSlider.gameObject.SetActive(true);
        do
        {
            timeSinceLastScore += Time.deltaTime;
            comboSlider.fillAmount = 1 - timeSinceLastScore / currentComboTime;
            yield return null;
        }
        while (timeSinceLastScore < currentComboTime);
        comboMultiplier = 0;
        comboText.enabled = false;
        comboSlider.gameObject.SetActive(false);
        timerIsActive = false;

    }
    public int IncreaseCombo()
    {
        comboText.text = "Combo x" + ++comboMultiplier;
        currentComboTime = maxComboTime - Mathf.Log(comboMultiplier) / 2;
        return comboMultiplier;
    }
    public IEnumerator ResolveMatch(Match toResolve,MatchType powerupUsed = MatchType.invalid)
    {
        Matchable powerupFormed = null;
        Matchable matchable;
        Transform target = collectionPoint;

        if (powerupUsed == MatchType.invalid && toResolve.count > 3)
        {
            powerupFormed = pool.UpgradeMatchable(toResolve.ToBeUpgraded, toResolve.Type);
            toResolve.RemoveMatchable(powerupFormed);
            target = powerupFormed.transform;
            powerupFormed.SortingOrder = 3;
            // play upgrade sound
            audioMixer.PlaySound(SoundEffects.upgrade);
        }
        else
        {
            audioMixer.PlaySound(SoundEffects.resolve);
        }

    /*    else if(toResolve.count == 4)
        {
           powerup = pool.UpgradeMatchable(toResolve.ToBeUpgraded, MatchType.match4);
            toResolve.RemoveMatchable(powerup);
            target = powerup.transform;
            powerup.SortingOrder = 3;
        }
        else if(toResolve.count > 4)
        {
            powerup = pool.UpgradeMatchable(toResolve.ToBeUpgraded, MatchType.match5);
            toResolve.RemoveMatchable(powerup);
            target = powerup.transform;
            powerup.SortingOrder = 3;
        }*/

        for (int i = 0; i != toResolve.count; ++i)
        {
            matchable = toResolve.Matchables[i];
            // check if this is amatch5 powerup, if it is, din't remove or resolve it!
            if (powerupUsed != MatchType.match5 && matchable.IsGem)
                continue;
            // remove the matchables form the grid
            grid.RemoveItemAt(matchable.position);
            // move them off to the side of thje screen
            if (i == toResolve.count - 1)
                yield return StartCoroutine(matchable.Resolve(target));
            else
                StartCoroutine(matchable.Resolve(target));

        }
        // update the player's score
        AddScore(toResolve.count * toResolve.count);
        if (powerupFormed != null)
            powerupFormed.SortingOrder = 0;
    }
}
