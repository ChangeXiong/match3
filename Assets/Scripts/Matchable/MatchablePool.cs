using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchablePool : ObjectPool<Matchable>
{
    [SerializeField] private int howManytypes;
    [SerializeField] private Sprite[] sprites;
    [SerializeField] private Color[] colors;
    [SerializeField] private Sprite match4Powerup;
    [SerializeField] private Sprite match5Powerup;
    [SerializeField] private Sprite crossPowerup;
    public void RandomizeType(Matchable toRandomize)
    {
        int random = Random.Range(0, howManytypes);
        toRandomize.SetType(random, sprites[random], colors[random]);
    }
    public Matchable GetRandomMatchable()
    {
        Matchable randomMatchable = GetPooledObject();
        RandomizeType(randomMatchable);

        return randomMatchable;
    }

    public int NextType(Matchable matchable)
    {
        int nextType = (matchable.Type + 1) % howManytypes;
        matchable.SetType(nextType, sprites[nextType], colors[nextType]);
        return nextType;
    }
    public Matchable UpgradeMatchable(Matchable toBeUpgraded, MatchType type)
    {
        if (type == MatchType.cross)
            return toBeUpgraded.Upgrade(MatchType.cross, crossPowerup);
        if(type == MatchType.match4)
       return toBeUpgraded.Upgrade(MatchType.match4, match4Powerup);
        
        if(type == MatchType.match5)
       return toBeUpgraded.Upgrade(MatchType.match5, match5Powerup);

        Debug.LogWarning("Tried to upgrade a matchable withan invalid match type.");
        return toBeUpgraded;
    }
    public void ChangeType(Matchable toChange, int type)
    {
        toChange.SetType(type, sprites[type], colors[type]);
    }
}
