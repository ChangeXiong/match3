using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MatchManager : Singleton<MatchManager>
{
    private MatchablePool pool;
    private MatchableGrid grid;
    private Cursor cursor;
    private AudiioMixer audioMixer;
    private ScoreManager score;

    [SerializeField] private Fader loadingScreen,darkener;
    [SerializeField] private Text finalScoreText;
    [SerializeField] private Movable resultsPage;
    [SerializeField] private bool levelIsTimed;
    [SerializeField] private LevelTimer timer;
    [SerializeField] private float timeLimit;

    [SerializeField] private Vector2Int dimensions = Vector2Int.one;
    [SerializeField] private Text gridOutput;
    [SerializeField] private bool debugMode;

    private void Start()
    {
        pool = (MatchablePool)MatchablePool.Instance;
        grid = (MatchableGrid)MatchableGrid.Instance;
        cursor = Cursor.Instance;
        audioMixer = AudiioMixer.Instance;
        score = ScoreManager.Instance;

       // pool.PoolObjects(10);
      //  grid.InitializeGrid(dimensions);
        // StartCoroutine(Demo());
        StartCoroutine(Setup());
    }
    private void Update()
    {
        if (debugMode && Input.GetButtonDown("Jump"))
            NoMoreMoves();
    }
    public void MoreScoreButton()
    {
        NoMoreMoves();
    }
    private IEnumerator Setup()
    {
        cursor.enabled = false;
        loadingScreen.Hide(false);
        // if level is timed, set the timer
        if (levelIsTimed)
            timer.SetTimer(timeLimit);

        pool.PoolObjects(dimensions.x * dimensions.y * 2);
        grid.InitializeGrid(dimensions);
        StartCoroutine(loadingScreen.Fade(0));
        audioMixer.PlayMusic();
        
        yield return StartCoroutine(grid.PopulateGrid(false,true));
        // then remove the loading screen down here
        grid.CheckPossibleMoves();

        // enable user input
        cursor.enabled = true;
        // if level is timed, start the timer
        if (levelIsTimed)
           StartCoroutine(timer.Countdown());
    }
    public void NoMoreMoves()
    {
        // If the level is timed, reward the player for running out of moves
        if(levelIsTimed)
            grid.MatchEverything();
        // In survival mode, punish the player for running out of moves
        else
            GameOver();
       
    }
    public void GameOver()
    {
        finalScoreText.text = score.Score.ToString();
        cursor.enabled = false;
        darkener.Hide(false);
        StartCoroutine(darkener.Fade(0.75f));
        StartCoroutine(resultsPage.MoveToPosition(new Vector2(Screen.width / 2, Screen.height / 2)));

    }
    private IEnumerator Demo()
    {
        gridOutput.text = grid.ToString();
        yield return new WaitForSeconds(2);
        /*Matchable m1 = pool.GetPooleObject();
        m1 = gameObject.SetActive(true);
        m1 = gameObject.name = "b";*/
        Matchable m1 = pool.GetPooledObject();
        m1.gameObject.SetActive(true);
        m1.gameObject.name = "a";
        Matchable m2 = pool.GetPooledObject();
        m2.gameObject.SetActive(true);
        m2.gameObject.name = "b";

        grid.PutItemAt(m1, 0, 1);
        grid.PutItemAt(m2, 0, 2);

        gridOutput.text = grid.ToString();
        yield return new WaitForSeconds(2);

        grid.SwapItemsAt(0, 1, 2, 3);
        gridOutput.text = grid.ToString();
        yield return new WaitForSeconds(2);

        grid.RemoveItemAt(0, 1);
        grid.RemoveItemAt(2, 3);
        gridOutput.text = grid.ToString();
        yield return new WaitForSeconds(2);

        pool.ReturnObjectToPool(m1);
        pool.ReturnObjectToPool(m2);

        yield return null;    
    }
    private IEnumerator Quit()
    {
        RankingScoreManager.SaveScore(ScoreManager.Instance.Score);
        yield return StartCoroutine(loadingScreen.Fade(1));
        SceneManager.LoadScene("Main Menu");
    }
    public void QuitButton()
    {
        StartCoroutine(Quit());
    }
    
    private IEnumerator Retry()
    {
        RankingScoreManager.SaveScore(ScoreManager.Instance.Score);
        StartCoroutine(resultsPage.MoveToPosition(new Vector2(Screen.width / 2, Screen.height / 2) + Vector2.down * 1000));
        yield return StartCoroutine(darkener.Fade(0));
        darkener.Hide(true);

        if (levelIsTimed)
            timer.SetTimer(timeLimit);

        cursor.Reset();
        score.Reset();
        yield return StartCoroutine(grid.Reset());
        cursor.enabled = true;

        if (levelIsTimed)
            StartCoroutine(timer.Countdown());

    }
    public void RetryButton()
    {
        StartCoroutine(Retry());
        
    }
  
}
