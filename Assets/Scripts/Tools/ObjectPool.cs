using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectPool<T> : Singleton<ObjectPool<T>> where T : MonoBehaviour
{
    [SerializeField] protected T prefab;

    private List<T> pooleObjects;
    private int amount;
    private bool isReady;

    // create the pool with a specified amount of objects

    public void PoolObjects(int amount = 0)
    {
        if (amount < 0)
            throw new ArgumentOutOfRangeException("Amount to pool must be non-negative.");
        this.amount = amount;
        pooleObjects = new List<T>();
        GameObject newObject;
        for(int i = 0; i != amount; ++i)
        {
            newObject = Instantiate(prefab.gameObject, transform);
            newObject.SetActive(false);
            pooleObjects.Add(newObject.GetComponent<T>());
        }
        isReady = true;
    }
    public T GetPooledObject()
    {
        if (!isReady)
            PoolObjects(1);
            for (int i = 0; i != amount; ++i)
                if (!pooleObjects[i].isActiveAndEnabled)
                    return pooleObjects[i];
        GameObject newObject = Instantiate(prefab.gameObject, transform);
        newObject.SetActive(false);
        pooleObjects.Add(newObject.GetComponent<T>());
        ++amount;

        return newObject.GetComponent<T>();
        
    }
    public void ReturnObjectToPool(T toBeReturned)
    {
        if (toBeReturned == null)
            return;
        if (!isReady)
        {
            PoolObjects();
            pooleObjects.Add(toBeReturned);
        }
        toBeReturned.gameObject.SetActive(false);
    }
}
