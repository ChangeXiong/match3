
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    private static T instance;

    public static T Instance
    {
        get
        {
            if (instance == null)
                Debug.Log("No instance of" + typeof(T) +"exists in the scene.");
            return instance;
        }
    }
    // create the refernce in Awake
    protected void Awake()
    {
        if(instance == null)
        {
            instance = this as T;
            Init();
        }else
        {
            Debug.Log("An instance of" + typeof(T) + "already exists in the scene. self-destruting.");
            Destroy(gameObject);
        }
    }
    // destory the reference in OnDestory
    protected void OnDestroy()
    {
        if (this == instance)
            instance = null;
    }
    protected virtual void Init()
    {

    }
}
